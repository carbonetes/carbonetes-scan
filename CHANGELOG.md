# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.4.2

- patch: Update api call to carbonetes analyzer.

## 1.4.1

- patch: Minimal changes on readme and metadata file.

## 1.4.0

- minor: Fix malware analysis service.

## 1.3.1

- patch: Fix the size conversion in BOM
- patch: Include Bill of Materials on the pipe descriptions

## 1.3.0

- minor: Add Bill Of Materials(BOM) to the analysis results
- minor: Add comments
- minor: Minimal changes on the metadata file

## 1.2.0

- minor: Add MIT LICENSE
- minor: Fix default value of FAIL_ON_POLICY
- minor: Malware Analysis is under maintenance, versions under this release may encounter errors on Malware Analysis
- minor: Minimal changes on README.md and metadata file

## 1.1.0

- minor: Add error messages.

## 1.0.1

- patch: Internal maintenance: fix yaml definition in the Readme.

## 1.0.0

- major: Initial release.
