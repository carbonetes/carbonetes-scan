#!/bin/sh

##########################################
# Required packages to execute the pipe
##########################################
apk add --update --no-cache curl
apk add --update --no-cache jq
apk add --update --no-cache coreutils
apk add --update --no-cache gawk

echo "Done installing required packages.."