#!/usr/bin/env bats

##########################################
# Setting up the PIPE IMAGE for testing
##########################################
setup() {
  PIPE_IMAGE=${DOCKER_IMAGE:="test/carbonetes-scan"}
  run docker build -t ${PIPE_IMAGE} .
}

teardown() {
    echo "Teardown happens after each test."
}

#####################
# Running the test
#####################
@test "Tests on docker image" {
    run docker container run \
        --env=REGISTRY_URI="$REGISTRY_URI" \
        --env=REPO_IMAGE_TAG="$REPO_IMAGE_TAG" \
        --env=CARBONETES_USERNAME="$CARBONETES_USERNAME" \
        --env=CARBONETES_PASSWORD="$CARBONETES_PASSWORD" \
        ${PIPE_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}
