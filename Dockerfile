FROM alpine:3.9

###########################################
# install bash to execute bash scripts
###########################################
RUN apk add --no-cache --upgrade bash

##############################################################
# installing all the required packages to execute the pipe
##############################################################
COPY packages /  
RUN chmod a+x /packages.sh
RUN /packages.sh

#############################
# Make the pipe executable
#############################
COPY pipe /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]