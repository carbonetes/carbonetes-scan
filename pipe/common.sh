#!/bin/bash

#############################
# Begin Standard 'imports'
#############################
set -e
set -o pipefail

############################################
# UTF-8 Strings for adding styles on texts
############################################
gray="\\e[37m"
blue="\\e[36m"
red="\\e[31m"
green="\\e[32m"
yellow="\\e[33m"
reset="\\e[0m"

###########################
# Displaying info message
###########################
info() { echo -e "${blue}INFO: $*${reset}"; }

############################
# Displaying error message
############################
error() { echo -e "${red}ERROR: $*${reset}"; }

##############################
# Displaying success message
##############################
success() { echo -e "${green}✔ $*${reset}"; }

#######################################################
# Displaying failed message and stopping the pipeline
#######################################################
fail() { echo -e "${red}✖ $*${reset}"; echo ""; exit 1; }

###########################
# Displaying warn message
###########################
warn() { echo -e "${yellow}WARNING: $*${reset}";}

###########################
# End standard 'imports'
###########################