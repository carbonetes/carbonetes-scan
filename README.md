# Bitbucket Pipelines Pipe: Carbonetes

Seamlessly integrates complete container analysis directly into your CI/CD pipeline.

## YAML Definition

Add the following **snippet** to the **script** section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: carbonetes/carbonetes-scan:1.4.2
  variables:
    CARBONETES_USERNAME: "<string>"
    CARBONETES_PASSWORD: "<string>"
    REGISTRY_URI: "<string>"
    REPO_IMAGE_TAG: "<string>"
    # FAIL_ON_POLICY: "<boolean>" # has a default value of true.
```

## Variables

| Variables                   | Usage                                                                                        |
| --------------------------- | -------------------------------------------------------------------------------------------- |
| CARBONETES_USERNAME \*      | The account username on Carbonetes. |
| CARBONETES_PASSWORD \*      | The account password on Carbonetes. |
| REGISTRY_URI \*             | The registry uri is managed in Carbonetes. |
| REPO_IMAGE_TAG \*           | The image to be scan. |
| FAIL_ON_POLICY              | Decides if it build or stop the build based on the policy evaluation. Default: `true`. |

_\* = required variable._

*Note : to be aligned in your CI/CD pipeline, make sure that you supply the same REPO_IMAGE_TAG that has been built within your pipeline stages.*

## Details

By merging Carbonetes into Bitbucket Pipelines, it can seamlessly analyze your code for vulnerabilities, software composition analysis (SCA), license types, malware, secrets, and bill of materials. It also check the results against the policy and automatically build or stop the build based on that policy evaluation.

Upon running the pipeline, the results are compared to the applicable policy to determine whether the container should build or not. The insight from the analysis and the policy evaluation are embedded right inside this pipe making it easy to find and resolve issues without ever leaving Bitbucket.

To discover more about Carbonetes, check [our website](https://carbonetes.com/).

## Prerequisites

This pipe requires a valid **Carbonetes credentials** `(email and password)`.

- Doesn't have any credentials yet? [Register now](https://carbonetes.com/).
- Add all the credentials inside the `repository variables` as [secured environment variables](https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/#Environmentvariables-Securedvariables).

## Example

### Docker image scan example
Uses Carbonetes to seamlessly integrate complete container analysis directly to analyze a Docker image.

```yaml
script:
    - docker build -t ${REPO_IMAGE_TAG} .
    
    - pipe: carbonetes/carbonetes-scan:1.4.2
      variables:
        CARBONETES_USERNAME: ${CARBONETES_USERNAME}
        CARBONETES_PASSWORD: ${CARBONETES_PASSWORD}
        REGISTRY_URI: ${REGISTRY_URI}
        #############################################################
        # Note: to be aligned in your CI/CD pipeline,
        #       make sure that you supply the same REPO_IMAGE_TAG
        #       that has been built within your pipeline stages.
        #############################################################
        REPO_IMAGE_TAG: ${REPO_IMAGE_TAG}

    - docker push ${REPO_IMAGE_TAG}
```

## Support
To help with this pipe, or have an issue or feature request, please contact: [eng@carbonetes.com](eng@carbonetes.com)

If reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License and Copyright

Copyright © 2021 Carbonetes

Licensed under [MIT License](https://bitbucket.org/carbonetes/carbonetes-scan/src/master/LICENSE).