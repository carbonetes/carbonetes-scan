[![Carbonetes](https://cdn.carbonetes.com/carbonetes-plugin/assets/branding/branding_header.png)](https://carbonetes.com)

# Carbonetes Complete Scan

## Carbonetes GitLab Integration

Contains an example .gitlab-ci.yml configurations to be added on a GitLab CI/CD.

Carbonetes Scan can be placed after the build stage and upon running the pipeline, Carbonetes Scan automatically initiates a complete container analysis. The results of that analysis are compared to the applicable policy to determine whether the container should build or not. The insight from the analysis and the policy evaluation are embedded right inside the Carbonetes Scan making it easy to find and resolve issues without ever leaving GitLab.

To discover more about Carbonetes, check [our website](https://carbonetes.com/).

This integration requires a valid **Carbonetes credentials** `(email and password)`.

- Doesn't have any credentials yet? [Register now](https://carbonetes.com/).
- Add all the credentials inside the `GitLab CI/CD environment variables` as [protected variables](https://docs.gitlab.com/ee/ci/variables/README.html#create-a-custom-variable-in-the-ui).

## Variables

| Variables                   | Usage                                                                                        |
| --------------------------- | -------------------------------------------------------------------------------------------- |
| CARBONETES_USERNAME \*      | The account username on Carbonetes. |
| CARBONETES_PASSWORD \*      | The account password on Carbonetes. |
| REGISTRY_URI \*             | The registry uri is managed in Carbonetes. |
| REPO_IMAGE_TAG \*           | The image to be scan. |

_\* = required variable._

*Note : to be aligned in your CI/CD pipeline, make sure that you supply the same REPO_IMAGE_TAG that has been built within your pipeline stages. See below sample pipeline stages.* 

![Pipeline Stages](assets/sample-pipeline-stages.png)

## Example CI/CD configurations

```yaml
variables:
  CARBONETES_PASSWORD: ${CARBONETES_PASSWORD:?'CARBONETES_PASSWORD environment variable is missing'}
  CARBONETES_USERNAME: ${CARBONETES_USERNAME:?'CARBONETES_USERNAME environment variable is missing.'}
  REGISTRY_URI: ${REGISTRY_URI:?'REGISTRY_URI environment variable is missing.'}
  REPO_IMAGE_TAG: ${REPO_IMAGE_TAG:?'REPO_IMAGE_TAG environment variable is missing.'}
  FAIL_ON_POLICY: ${FAIL_ON_POLICY:="true"}

stages:
- build
- scan
- publish

container_build:
  stage: build
  image: docker:stable
  services:
  - docker:stable-dind

  script:
  - docker build -t "$REPO_IMAGE_TAG" .

carbonetes-complete-scan:
  image: docker:stable
  stage: scan
  services:
    - docker:stable-dind
  before_script:
    #####################################
    # Recommended image tag for GitLab
    #####################################
    - docker pull carbonetes/carbonetes-scan:latest
  script:
    #############################################################
    # Execution of Carbonetes Complete Scan
    # Note: to be aligned in your CI/CD pipeline,
    #       make sure that you supply the same REPO_IMAGE_TAG
    #       that has been built within your pipeline stages.
    #############################################################
    - |
      docker container run \
        --env=REGISTRY_URI="$REGISTRY_URI" \
        --env=REPO_IMAGE_TAG="$REPO_IMAGE_TAG" \
        --env=CARBONETES_USERNAME="$CARBONETES_USERNAME" \
        --env=CARBONETES_PASSWORD="$CARBONETES_PASSWORD" \
        --env=FAIL_ON_POLICY="$FAIL_ON_POLIY" \
        carbonetes/carbonetes-scan:latest
      

container_publish:
  stage: publish
  image: docker:stable
  services:
  - docker:stable-dind

  script:
  - docker tag "$REPO_IMAGE_TAG" "${CI_REGISTRY_IMAGE}:latest"
  - docker push "${CI_REGISTRY_IMAGE}:latest"
```

### Results provided by Carbonetes Scan

After Carbonetes finishes scanning the image, by default, the following results are generated.
* `Vulnerabilities` - A list of known security risks that can be exploit by a threat actor listed with severities.
* `Software Compositions` - Software that might cause a security risk listed with severities.
* `Software Dependencies` - Pieces of software that relies on each other listed with vulnerability counts.
* `Licenses` - Legal compliance found on each software of the scanned image.
* `Malware` - Virus threats found on the scanned image.
* `Secrets` - Secret data found on each software of the scanned image.
* `Bill of Materials` - A list of all the components exist in a software.
* `Policy Result` - the result of the policy evaluation, `PASSED` or `FAILED`.
* `Final Action` - decide if the build will `STOP` or `GO` based on the policy evaluation.

## Support
To help with this integration, or have an issue or feature request, please contact: [eng@carbonetes.com](eng@carbonetes.com)

If reporting an issue, please include:

* the carbonetes-scan image tag that you've used
* relevant logs and error messages
* steps to reproduce

## License and Copyright

Copyright © 2021 Carbonetes

Licensed under [MIT License](LICENSE).